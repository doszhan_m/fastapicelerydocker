
from celery.result import AsyncResult
from fastapi import Body, FastAPI, Form, Request
from fastapi.responses import JSONResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates

from worker import create_task


app = FastAPI()
app.mount("/static", StaticFiles(directory="static"), name="static")
templates = Jinja2Templates(directory="templates")


@app.get("/")
def home(request: Request):
    '''Main page'''
    return templates.TemplateResponse("home.html", context={"request": request})


@app.post('/tasks')
def run_task(payload: int = Body(...)):
    task_duration = payload
    task = create_task.delay(task_duration)
    result = {'task_id': task.id}
    return result 
